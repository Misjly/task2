﻿using System.Collections.Generic;
using Task2.TextObjects;

namespace Task2
{
    interface IText
    {
        IList<ISentence> Sentences { get; set; }
        int SentenceNumber => Sentences.Count;
        string TextValue { get; }
        Text AscendingOrder();
        Text SubstringReplacement(int number, int wordsLength, string substring);
    }
}
