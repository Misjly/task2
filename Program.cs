﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Task2.Loggers;
using Task2.TextObjects;
using Task2.TextObjects.SentenceObjects.SentenceInterfaces;

/*1. Вывести все предложения заданного текста в порядке возрастания количества слов в каждом из них.
2. Во всех вопросительных предложениях текста найти и напечатать без повторений слова заданной длины.
3. Из текста удалить все слова заданной длины, начинающиеся на согласную букву.
4. В некотором предложении текста слова заданной длины заменить указанной подстрокой, длина которой
может не совпадать с длиной слова.*/
namespace Task2
{
    class Program
    {
        static void Main()
        {
            ILogger logger = new ConsoleLogger();


            var builder = new ConfigurationBuilder();
            builder.AddJsonFile($"appconfig.json", true, true);
            IConfiguration configuration = builder.Build();


            //var title = configuration["Filepath"];


            string fileName = configuration["Filepath"];
            Console.OutputEncoding = Encoding.UTF8;

            FileStream file = null;
            try
            {
                file = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            }
            catch (Exception)
            {
                logger.WriteLine($"Не удалось открыть файл {fileName}");
                return;
            }
            var sr = new StreamReader(file);


            IText text = new Text(sr);
            logger.WriteLine(text.TextValue);
            logger.WriteLine();
            logger.WriteLine();

            IText ascendingSentences = text.AscendingOrder();   //Вывести все предложения заданного текста в порядке
            logger.WriteLine(ascendingSentences.TextValue);     //возрастания количества слов в каждом из них.
            logger.WriteLine();


            //Во всех вопросительных предложениях текста найти и напечатать без повторений слова заданной длины.
            logger.WriteLine("Введите длину для поиска слов в вопросительных предложениях");
            int length = Convert.ToInt32(Console.ReadLine());
            bool flag = false;

            foreach (var sentence in text.Sentences.Where(e => e.IsQuestion == true))
            {
                IList<IWord> words = sentence.PickSameLengthWords(length);

                foreach (var word in sentence.Items.OfType<IWord>())
                {
                    if (words.Contains(word))
                    {
                        flag = true;
                        if (sentence.IsWordUnique(word))
                            while (words.Remove(word)) ;
                        logger.WriteLine(word.Chars);
                    }
                }
            }

            if (!flag)
                logger.WriteLine("Нет слов такой длины в вопросительных предложениях, либо нет вопросительных предложений");

            logger.WriteLine();


            logger.WriteLine("Введите длину для удаления слов начинающихся на согласную");
            length = Convert.ToInt32(Console.ReadLine());

            foreach (ISentence sentence in text.Sentences)
                sentence.DeleteСonsonantStartingSameLengthWords(length);

            //Из текста удалить все слова заданной длины, начинающиеся на согласную букву.
            logger.WriteLine(text.TextValue);
            logger.WriteLine();


            int number;
            do
            {
                logger.WriteLine("Введите номер предложения для изменения в нём подстроки");
                number = Convert.ToInt32(Console.ReadLine());
                if (number > text.SentenceNumber || number < 1)
                    logger.WriteLine("Неверный ввод");
            } while (number > text.SentenceNumber || number < 1);
            number--;

            int wordsLength;
            do
            {
                logger.WriteLine("Введите длину слов для замены");
                wordsLength = Convert.ToInt32(Console.ReadLine());
                if (wordsLength < 1)
                    logger.WriteLine("Неверный ввод");
            } while (wordsLength < 1);
            logger.WriteLine("Введите подстроку"); //В некотором предложении текста слова заданной длины заменить
            string substring = Console.ReadLine(); //указанной подстрокой, длина которой может не совпадать с длиной слова.
            IText newText = text.SubstringReplacement(number, wordsLength, substring);
            logger.WriteLine(newText.TextValue);
        }
    }
}

