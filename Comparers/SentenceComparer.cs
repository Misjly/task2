﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Task2.TextObjects;

namespace Task2.Comparers
{
    public class SentenceComparer : IComparer<ISentence>
    {
        public int Compare([AllowNull] ISentence x, [AllowNull] ISentence y)
        {
            if (x.WordNum > y.WordNum)
                return 1;
            if (x.WordNum < y.WordNum)
                return -1;
            return 0;
        }
    }
}
