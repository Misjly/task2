﻿using System.IO;

namespace Task2.Loggers
{
    public class FileLogger : ILogger
    {
        private readonly StreamWriter _streamWriter;

        public FileLogger(string fileName)
        {
            var file = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
            _streamWriter = new StreamWriter(file);
        }

        public void Dispose()
        {
            _streamWriter?.Dispose();
        }

        public void WriteLine(string message = null)
        {
            _streamWriter.WriteLine(message);
        }

        public void Write(string message = null)
        {
            _streamWriter.Write(message);
        }
    }
}
