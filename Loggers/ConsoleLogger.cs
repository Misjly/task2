﻿using System;

namespace Task2.Loggers
{
    public class ConsoleLogger : ILogger
    {
        public void WriteLine(string message = null)
        {
            Console.WriteLine(message);
        }

        public void Write(string message = null)
        {
            Console.Write(message);
        }
    }
}
