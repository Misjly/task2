﻿namespace Task2.Loggers
{
    interface ILogger
    {
        void Write(string message = null);
        void WriteLine(string message = null);
    }
}
