﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Task2.TextObjects;

namespace Task2.Parsers
{
    public class FileParser : IParser
    {
        private readonly StreamReader _streamReader;

        public FileParser(StreamReader streamReader)
        {
            _streamReader = streamReader;
        }

        public void Parse(Text text)
        {
            text.Sentences = new List<ISentence>();
            var regex = new Regex(@"[\s\S]+?[\.\?!]+");
            MatchCollection matches = regex.Matches(_streamReader.ReadToEnd());

            foreach (Match match in matches)
            {
                ISentence tempSentence = new Sentence(match.Value.Replace("\n", ""));
                text.Sentences.Add(tempSentence);
            }
        }
    }
}
