﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Task2.TextObjects;

namespace Task2.Parsers
{
    public class ConsoleParser : IParser
    {
        private readonly string _text;
        public ConsoleParser(string textValue)
        {
            _text = textValue;
        }
        public void Parse(Text text)
        {
            text.Sentences = new List<ISentence>();
            var regex = new Regex(@"[\s\S]+?[\.\?!]+");
            MatchCollection matches = regex.Matches(_text);

            foreach (Match match in matches)
            {
                ISentence tempSentence = new Sentence(match.Value.Replace("\n", ""));
                text.Sentences.Add(tempSentence);
            }
        }
    }
}
