﻿namespace Task2.Parsers
{
    public interface IParser
    {
        void Parse(Text text);
    }
}
