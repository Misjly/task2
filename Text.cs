﻿using System.Collections.Generic;
using System.Text;
using System.IO;
using Task2.TextObjects;
using Task2.Comparers;
using Task2.TextObjects.SentenceObjects.SentenceInterfaces;
using System.Linq;
using Task2.Parsers;

namespace Task2
{
    public class Text : IText
    {
        public IList<ISentence> Sentences { get; set; }
        public int SentenceNumber => Sentences.Count;
        public string TextValue
        {
            get
            {
                var tempString = new StringBuilder();

                if (Sentences != null)
                {
                    foreach (var item in Sentences)
                    {
                        tempString.Append(item.SentenceValue + " ");
                    }
                }
                return tempString.ToString();
            }
        }

        public Text(string textValue)
        {
            IParser fileParser = new ConsoleParser(textValue);
            fileParser.Parse(this);
        }


        public Text(StreamReader sr)
        {
            IParser fileParser = new FileParser(sr);
            fileParser.Parse(this);
        }


        public Text() { }


        public Text AscendingOrder()
        {
            var ascendingSentences = new List<ISentence>(Sentences);
            ascendingSentences.Sort(new SentenceComparer());
            var ascendingString = new StringBuilder();
            foreach (ISentence sentence in ascendingSentences)
                ascendingString.Append(sentence.SentenceValue);
            var text = new Text();
            return text;
        }


        public Text SubstringReplacement(int number, int wordsLength, string substring)
        {
            var text = new Text(TextValue);

            foreach (var item in text.Sentences[number].Items.OfType<IWord>().Where(e => e.Length == wordsLength))
            {
                item.Symbols = new Word(substring).Symbols;
            }

            return text;
        }
    }
}
