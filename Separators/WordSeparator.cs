﻿using System.Text.RegularExpressions;

namespace Task2.Separators
{
    public static class WordSeparator
    {
        public static string[] RussianVowels { get; } = { "а", "у", "о", "ы", "и", "э", "я", "ю", "ё", "е" };
        public static string[] EnglishVowels { get; } = { "a", "e", "y", "u", "i", "o"};
        public static Regex WordSignature { get; } = new Regex(@"\w+");
    }
}
