﻿using System.Text.RegularExpressions;

namespace Task2.Separators
{
    public static class PunctuationSeparator
    {
        public static Regex RgxSigns { get; } = new Regex(@"[\.,\?!:;\']");
        public static string[] Signs { get; } = new string[] { @"\.", ",", @"\?", "!", ":", ";",  @"\'"};
        public static string[] SignsForReplacement { get; } = new string[] { ".", ",", "?", "!", ":", ";", @"\'" };
    }
}
