﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Task2.Separators;
using Task2.TextObjects;
using Task2.TextObjects.SentenceObjects.SentenceInterfaces;

namespace Task2
{
    public class Sentence : ISentence
    {
        public int WordNum => Items.Count;
        public bool IsQuestion { get; } = false;
        public IList<ISentenceItem> Items { get; set; }
        public string SentenceValue
        {
            get
            {
                var tempString = new StringBuilder();
                foreach (var item in Items)
                {
                    tempString.Append(" " + item.Chars);
                }
                return FixSpaces(tempString.ToString());
            }
            set
            {
                SentenceValue = value;
            }
        }


        public Sentence(string str)
        {
            Regex regex = new Regex(PunctuationSeparator.RgxSigns + "|" + WordSeparator.WordSignature);
            MatchCollection matches = regex.Matches(str);
            Items = new List<ISentenceItem>();

            foreach (Match match in matches)
            {
                Items.Add(SentenceItemFactory.CreateSentenceItem(match.Value));
                if (Items[Items.Count - 1].Chars == "?")
                    IsQuestion = true;
            }
        }

        private string FixSpaces(string sentenceValue)
        {
            var regex = new Regex(@"\t");
            sentenceValue = regex.Replace(sentenceValue, " ");
            regex = new Regex(@"[ ]{2,}");
            sentenceValue = regex.Replace(sentenceValue, " ");
            for (int i = 0; i < PunctuationSeparator.Signs.Length; i++)
            {
                regex = new Regex("[ ]" + PunctuationSeparator.Signs[i]);
                sentenceValue = regex.Replace(sentenceValue, PunctuationSeparator.SignsForReplacement[i]);
            }
            return sentenceValue;
        }

        public bool IsWordUnique(IWord specialWord)
        {
            int count = -1;
            foreach (var item in Items)
            {
                if (specialWord.Chars.ToLower() == item.Chars.ToLower())
                    count++;
            }
            return Convert.ToBoolean(count);
        }

        public IList<IWord> PickSameLengthWords(int length)
        {
            List<IWord> itemList = new List<IWord>();
            itemList.AddRange(Items.OfType<IWord>().Where(e => e.Length == length));

            return itemList;
        }

        public void DeleteСonsonantStartingSameLengthWords(int length)
        {
            int index = 0;
            ISentence tempSentence = new Sentence(SentenceValue);
            foreach (var item in tempSentence.Items.OfType<Word>().Where(e => e.Length == length))
            {
                if ((item as Word).IsConsonantStarting)
                {
                    Items.RemoveAt(index);
                    index--;
                }
                index++;
            }
        }
    }
}
