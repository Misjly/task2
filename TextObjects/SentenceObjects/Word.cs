﻿using System.Linq;
using System;
using Task2.TextObjects.SentenceObjects.SentenceInterfaces;
using Task2.Separators;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Collections.Generic;

namespace Task2
{
    public class Word : IWord
    {
        public IList<Symbol> Symbols { get; set; }
        public string Chars
        {
            get
            {
                var tempString = new StringBuilder();
                foreach(var symbol in Symbols)
                {
                    tempString.Append(symbol.Chars);
                }
                return tempString.ToString();
            }

        }
        public int Length { get; }
        public bool IsConsonantStarting { get; set; } = false;

        public Word(string str)
        {
            Symbols = str?.Select(x => new Symbol(x)).ToList();
            Length = Chars.Length;

            char c = char.ToUpper(Chars[0]);

            IsConsonantStarting = ConsonantStartingCheck(WordSeparator.RussianVowels) || ConsonantStartingCheck(WordSeparator.EnglishVowels);
        }

        private bool ConsonantStartingCheck(string[] vowels)
        {
            return !vowels.Contains(Symbols[0].Chars);
        }

        public bool Equals([AllowNull] IWord other)
        {
            return other != null && Enumerable.SequenceEqual(Chars, other.Chars);
        }
    }
}
