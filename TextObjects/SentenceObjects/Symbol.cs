﻿using System.Diagnostics.CodeAnalysis;
using Task2.TextObjects.SentenceObjects.SentenceInterfaces;

namespace Task2
{
    public struct Symbol : ISymbol
    {
        public string Chars { get; }

        public Symbol(string @char)
        {
            Chars = @char;
        }
        public Symbol(char @char)
        {
            Chars = $"{@char}";
        }

        public bool Equals([AllowNull] ISymbol other)
        {
            return Chars == other.Chars;
        }
    }
}
