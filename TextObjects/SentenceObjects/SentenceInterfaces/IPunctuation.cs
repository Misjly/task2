﻿namespace Task2.TextObjects.SentenceObjects.SentenceInterfaces
{
    interface IPunctuation : ISentenceItem
    {
        Symbol SymbolValue { get; set; }
    }
}
