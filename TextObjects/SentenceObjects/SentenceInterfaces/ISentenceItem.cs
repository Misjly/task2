﻿namespace Task2.TextObjects.SentenceObjects.SentenceInterfaces
{
    public interface ISentenceItem
    {
        public string Chars { get; }
    }
}
