﻿using System;
using System.Collections.Generic;

namespace Task2.TextObjects.SentenceObjects.SentenceInterfaces
{
    public interface IWord : ISentenceItem, IEquatable<IWord>
    {
        IList<Symbol> Symbols { get; set; }
        int Length { get; }
        bool IsConsonantStarting { get; set; }
    }
}
