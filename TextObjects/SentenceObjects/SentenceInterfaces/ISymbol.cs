﻿using System;

namespace Task2.TextObjects.SentenceObjects.SentenceInterfaces
{
    public interface ISymbol : ISentenceItem, IEquatable<ISymbol> { }
}
