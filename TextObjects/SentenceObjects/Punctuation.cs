﻿using Task2.TextObjects.SentenceObjects.SentenceInterfaces;

namespace Task2
{
    public class Punctuation : IPunctuation
    {
        public Symbol SymbolValue { get; set; }
        public string Chars => SymbolValue.Chars;

        public Punctuation(string Char)
        {
            SymbolValue = new Symbol(Char);
        }
        public Punctuation() { }
    }
}
