﻿using Task2.Separators;
using Task2.TextObjects.SentenceObjects.SentenceInterfaces;

namespace Task2.TextObjects
{
    public static class SentenceItemFactory
    {
        public static ISentenceItem CreateSentenceItem(string sentenceItem)
        {
            if (WordSeparator.WordSignature.IsMatch(sentenceItem))
                return new Word(sentenceItem);

            if (PunctuationSeparator.RgxSigns.IsMatch(sentenceItem))
                return new Punctuation(sentenceItem);


            return new Symbol(sentenceItem);
        }
    }
}
