﻿using System.Collections.Generic;
using Task2.TextObjects.SentenceObjects.SentenceInterfaces;

namespace Task2.TextObjects
{
    public interface ISentence
    {
        IList<ISentenceItem> Items { get; set; }
        int WordNum => Items.Count;
        bool IsQuestion { get; }
        string SentenceValue { get; }
        bool IsWordUnique(IWord specialWord);
        IList<IWord> PickSameLengthWords(int length);
        void DeleteСonsonantStartingSameLengthWords(int Length);
    }
}
